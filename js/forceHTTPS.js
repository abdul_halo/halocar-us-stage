
(() => {
    if(location.origin.includes('localhost')) return;
    if (location.protocol != 'https:'){
        location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
    }
})()
