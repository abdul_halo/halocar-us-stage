function resizeIFrameToFitContent() {
    var iFrame = document.getElementById('survey-iframe');
    document.getElementById('halo-navbar').style.display = 'none';
    document.getElementById('halo-footer').style.display = 'none';
    iFrame.src = document.iFrameSrc;
    iFrame.width = window.innerWidth;
    iFrame.height = window.innerHeight;
}