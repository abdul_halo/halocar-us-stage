function $id(id) {
	return document.getElementById(id)
}

// asyncrhonously fetch the html template partial from the file directory,
// then set its contents to the html of the parent element
function loadHTML(url, id, callback) {
	req = new XMLHttpRequest()
	req.open('GET', url)
	req.send()
	req.onload = function() {
		$id(id).innerHTML = req.responseText
		if (callback) callback()
	}
}

function loadHome() {
	loadHTML('pages/home.html', 'content', hubspotModalInit)
}

function hubspotModalInit() {
	$(document).ready(function() {
		loadHubspot()
	})

	$(document).on('click', '.hubspot-button', function() {
		$('#hubspot-container').show()
		$('#hubspot-container').modal({ backdrop: true })

		$('body').on('click', function() {
			$('#hubspot-container').hide()
			// $('#hubspot-container').modal({ backdrop: false })
		})

		$('#hubspot-container .modal-footer').on('click', function() {
			$('hubspot-container').hide()
		})

		$('#hubspot-container .modal').on('click', function(e) {
			e.stopPropagation()
		})
	})
}

function loadHubspot() {
	$('#hubspot-iframe').append(
		'<div class="meetings-iframe-container" data-src="https://app.hubspot.com/meetings/tyler103?embed=true"></div>',
	)
	$.getScript(
		'https://static.hsappstatic.net/MeetingsEmbed/ex/MeetingsEmbedCode.js',
	)
}

function parseQS(query) {
	var result = {}
	query
		.split('&') //split string into ['queryparam=value']
		.forEach(function(qsPair) {
			var splitQsPair = qsPair.split('=') //split 'queryparam=value' into [q,v]
			result[splitQsPair[0]] = splitQsPair[1]
		})
	return result
}

// use #! to hash
var router = new Navigo(null, false)
router.on({
	// 'view' is the id of the div element inside which we render the HTML
	'/': loadHome,
	// '/': () => { loadHTML('pages/home.html', 'view') },
	'/dealer': function() {
		loadHTML('pages/dealer.html', 'view')
	},
	'/oem': function() {
		loadHTML('pages/oem.html', 'view')
	},
	'/survey': function(params, query) {
		//query = hello=hi&what=yo
		// localhost: 8000 / survey ? Name = Ben & Surname= Spenard & Email=bdspen@gmail.com& Phone=5032671143
		// 'https://www.surveymonkey.com/r/SMZGMT8?Name=[Name_value]&Surname=[Surname_value]&Email=[Email_value]&Phone=[Phone_value]'
		loadHTML('pages/survey.html', 'view', function() {
			// document.iFrameSrc = 'https://www.surveymonkey.com/r/SMZGMT8?' + query;
			document.iFrameSrc = 'https://www.surveymonkey.com/r/KNQVN68' + query

			// document.parsedQuery = parseQS(query);
			resizeIFrameToFitContent()
		})
	},
	'/letschat': function() {
		loadHTML('pages/letschat.html', 'view')
	},
	'/support': function() {
		loadHTML('pages/support.html', 'view')
	},
	'/home': function() {
		loadHTML('pages/home.html', 'view')
	},
	'/dealership': function() {
		loadHTML('pages/dealership.html', 'view')
	},
	'/resources': function() {
		loadHTML('pages/resources.html', 'view')
	},
})

// set the default route
router.on(function() {
	$('#hubspot-container').hide()
	loadHome()
})

// set the 404 route
router.notFound(query => {
	$id('view').innerHTML =
		"<h3>Couldn't find the page you're looking for...</h3>"
})

router.resolve()
