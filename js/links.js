function generateNavbarlinks() {
	var navList = document.getElementById('nav-links')
	var content = ''

	if (!includes('home')) {
		content += '<a href="/">home</a>'
	}
	// if (!includes('resources')) {
	// 	content += '<a href="resources">resources</a>'
	// }
	if (!includes('dealership')) {
		content += '<a href="dealership">dealerships</a>'
	}

	navList.innerHTML = content
}

function includes(value, array) {
	if (!array) return window.location.pathname.includes(value)
	else
		return value.filter(function(val) {
			return window.location.pathname.includes(val)
		}).length
}

window.onload = generateNavbarlinks
