function requestDemo() {

    var demoButtonEm = document.getElementById("demo-button");
    demoButtonEm.disabled = true;

    var dealerNameEm = document.getElementById("dealer-name");
    var dealerEmailEm = document.getElementById("dealer-email");
    var http = new XMLHttpRequest();

    navigator.geolocation.getCurrentPosition(function (position) {
        request(position);
    },
    function(err){
        request();
    });

    return false;

    function request(position){

        var params = {
            replyTo: dealerEmailEm.value,
            subject: 'Dealer Demo Request for ' + dealerNameEm.value,
            name: dealerNameEm.value,
            referrer: document.referrer,
            host: window.location.href,
            geolocation: "Opted Out",
            system: navigator.appVersion,
            platform: navigator.platform
        };

        if(position) {
            params.geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
        }

        http.open("POST", 'https://dealers1dev.herokuapp.com/terms/feedback?type=dealerDemo&sendTo=sales@halocar.us');
        http.setRequestHeader("Content-type", "application/json");
    
        http.onreadystatechange = function (oEvent) {
            if (http.readyState === 4) {
                if (http.status === 200) {
                    console.log(http.responseText);
                    dealerNameEm.value = '';
                    dealerEmailEm.value = '';
                    showResponse('Your request has been sent!', 'success');
                } else {
                    showResponse(http.statusText, 'error');
                    console.log("Error", http.statusText);
                }
                demoButtonEm.disabled = false;
            }
        };
        http.send(JSON.stringify(params));
    }


}

function letsChat() {

    var chatButtonEm = document.getElementById("chat-button");
    chatButtonEm.disabled = true;

    var nameEm = document.getElementById("chat-name");
    var emailEm = document.getElementById("chat-email");
    var messageEm = document.getElementById("chat-message");
    var checkBoxEm = document.getElementById("copy-checkbox");
    var http = new XMLHttpRequest();

    var params = {
        replyTo: emailEm.value,
        name: nameEm.value,
        message: messageEm.value,
        sendCopy: checkBoxEm.checked,
        subject: 'Support Request for ' + nameEm.value,
    };
    http.open("POST", 'https://dealers1dev.herokuapp.com/terms/feedback?type=letsChat&sendTo=support@halocar.us');
    http.setRequestHeader("Content-type", "application/json");

    http.onreadystatechange = function (oEvent) {
        if (http.readyState === 4) {
            if (http.status === 200) {
                console.log(http.responseText);
                nameEm.value = '';
                emailEm.value = '';
                messageEm.value = '';
                checkBoxEm.checked = 'false';
                showResponse('Your request has been sent!', 'success');
            } else {
                showResponse(http.statusText, 'error');
                console.log("Error", http.statusText);
            }
            chatButtonEm.disabled = false;
        }
    };
    http.send(JSON.stringify(params));

    return false;

}

function support() {

    var chatButtonEm = document.getElementById("support-submit");
    chatButtonEm.disabled = true;

    var nameEm = document.getElementById("support-name");
    var emailEm = document.getElementById("support-email");
    // var subjectEm = document.getElementById("support-subject");
    var descriptionEm = document.getElementById("support-description");
    var vinEm = document.getElementById("support-vin");
    var http = new XMLHttpRequest();

    var params = {
        name: nameEm.value,
        email: emailEm.value,
        // subject: subjectEm.value,
        vin: vinEm.value || "none",
        description: descriptionEm.value,
        subject: 'Support Request for ' + nameEm.value,
    };
    http.open("POST", "https://dealers1dev.herokuapp.com/terms/feedback/support");
    http.setRequestHeader("Content-type", "application/json");

    http.onreadystatechange = function (oEvent) {
        if (http.readyState === 4) {
            if (http.status === 200) {
                console.log(http.responseText);
                nameEm.value = '';
                emailEm.value = '';
                // subjectEm.value = '';
                descriptionEm.value = '';
                vinEm.value = '';
                showResponse('Your request has been sent!', 'success');
            } else {
                showResponse(http.statusText + " please try again or email support@halocar.us", 'error');
                console.log("Error", http.statusText);
            }
            chatButtonEm.disabled = false;
        }
    };
    http.send(JSON.stringify(params));

    return false;

}

function showResponse(response, type){

    var responseDiv = document.getElementById("alert-container");
    var messageDiv = document.getElementById("alert-message");

    if(type === 'error'){
        if(!response) response = "There was an error, please try again.";
        messageDiv.className += " alert-danger";
    }
    else messageDiv.className += " alert-success";

    responseDiv.style.visibility = 'visible';
    messageDiv.innerText = response;

    // clear alert after x amount of time
    setTimeout(function(){
        messageDiv.innerText = '';
        responseDiv.style.visibility = 'hidden';
    }, 3500)

}